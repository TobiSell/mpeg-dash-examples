use Qencode\Classes\CustomTranscodingParams;
use Qencode\QencodeApiClient;

require 'vendor/autoload.php';

$q = new QencodeApiClient("API TOKEN");

$task = $q->createTask();
$json=file_get_contents("task.json");
$task->startCustom($json);

$response = $task->getStatus();
var_dump($response);
